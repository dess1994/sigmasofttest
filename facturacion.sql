-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-12-2018 a las 00:20:21
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `valor` float NOT NULL,
  `descuento` float NOT NULL,
  `iva` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id`, `idventa`, `idproducto`, `descripcion`, `valor`, `descuento`, `iva`, `total`) VALUES
(1, 11, 1, 'Arroz', 1100, 22.22, 11, 1088.78),
(2, 12, 4, 'Harina de Trigo', 900, 36.72, 18, 881.28),
(3, 12, 7, 'Mayonesa', 3600, 587.52, 288, 14100.5),
(4, 12, 9, 'Mostaza', 2400, 97.92, 48, 2350.08),
(5, 13, 1, 'Arroz', 1100, 0, 0, 3300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `valor` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `descripcion`, `valor`) VALUES
(1, 'Arroz', 1100),
(2, 'Pasta', 2800),
(3, 'Harina de Maiz', 3100),
(4, 'Harina de Trigo', 900),
(5, 'Huevos', 8000),
(6, 'Salsa de Tomate', 2400),
(7, 'Mayonesa', 3600),
(8, 'Atun', 5350),
(9, 'Mostaza', 2400),
(10, 'Leche', 4700);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `consecutivo` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `fecha` text NOT NULL,
  `hora` text NOT NULL,
  `subtotal` float NOT NULL,
  `descuento` float NOT NULL,
  `iva` float NOT NULL,
  `totalneto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `consecutivo`, `idcliente`, `fecha`, `hora`, `subtotal`, `descuento`, `iva`, `totalneto`) VALUES
(11, 11, 2, '2018-12-22', '16:55:29', 1100, 22.22, 11, 1088.78),
(12, 12, 2, '2018-12-22', '16:56:13', 17700, 722.16, 354, 17331.8),
(13, 13, 1, '2018-12-22', '16:58:59', 3300, 0, 0, 3300);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
