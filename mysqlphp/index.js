
var h;
var productos_cola=[];
var suma=0;
var descuento=0;
var iva=0;
var totalPagar=0;
var subtotal=0;
var id_cliente=0;
var totalIva = 0;
var totalDescuento = 0;

$(document).ready(function(){

	getProducts();
   

}) 
	//funcion para encolar los productos
	function encolar(){
		var product = $('#selProd').val();
		var name= $('#selProd option:selected').text();
		var price = $('#selProd option:selected').attr('data-precio');
		var cant = $('#cantidad').val();
		productos_cola.push({
			'product':product,
			'name':name,
			'price':price,
			'cant':cant,
			'descuento':0,
			'iva':0,
			'total':(cant*price)
		});
		
		console.log(productos_cola);
	}

	//Mostrar impuestos
	function impuestos(){
		id_cliente=$('#id_client').val();
		$('#registro').css('display','none');
		$('#impuestos').css('display', 'block');
	}

	//Calculos de impuestos-descuentos y mostrar lista
	function calculo_cuenta(){
		iva = $('#iva').val();
		descuento = $('#descuento').val();
		 totalIva = 0;
		var nuevoVal = suma+totalIva;
		totalDescuento = 0;
		totalPagar = 0;
		subtotal=0;

		for(var i=0; i<productos_cola.length; i++){
			productos_cola[i].iva=(productos_cola[i].total*(iva/100));
			totalIva+=productos_cola[i].iva;
			productos_cola[i].total+=productos_cola[i].iva;
			productos_cola[i].descuento=(productos_cola[i].total*(descuento/100));
			totalDescuento+=productos_cola[i].descuento;
			productos_cola[i].total-=productos_cola[i].descuento;
			subtotal+=(productos_cola[i].price*productos_cola[i].cant);
			totalPagar+=productos_cola[i].total;
		
			$('#tablaGen').append('<tr><td>'+productos_cola[i].name+'</td><td style="text-align:center;">'+productos_cola[i].cant+'</td><td style="text-align:center">'+productos_cola[i].iva+'</td><td style="text-align:center">'+productos_cola[i].descuento+'</td><td>'+(productos_cola[i].total)+'$</td>');
		}
		$('#val_sub').text('subtotal: '+subtotal);
		$('#val_iva').text('total iva: '+totalIva);
		$('#val_desc').text('total descuento: '+totalDescuento);
		$('#val_pagar').text('Total Pagar: '+totalPagar);
		$('#impuestos').css('display','none');
		$('#detalleFactura').css('display','block');
	}

	function registrarPago(){
		return $.ajax({
			url:"servicios.php",
			type:'post',
			data:{'funcion':'postVentas', 'datos':productos_cola, 'total':totalPagar, 'id_cliente':id_cliente, 'subtotal':subtotal, 'totaldesc':totalDescuento, 'total_iva':totalIva}
		}).done(function(respuesta){
			h=respuesta;
				console.log(h);
		});
	}


	// getproducts para traer productos  
	function getProducts() {
		
       return $.ajax({
        	url:"servicios.php",
        	method:'GET',
        	async:false,
        	dataType:"json",
        	data:{funcion:"getProducts"}
        }).done(function(respuesta){
        	h = respuesta;
        	 for(var i=0; i<h.length; i++){
        	 	$("#selProd").append('<option data-precio='+h[i].valor+' value='+h[i].id+'>'+h[i].descripcion+'</option>');
        	 }
        });  
    }