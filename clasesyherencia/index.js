function mostrarClase(){
	$('#clase').css('display','block');
	$('#opciones').css('display','none');
}

function mostrarHerencia(){
	$('#clase').css('display','block');
	$('#herencia').css('display','block');
	$('#calcularClase').css('display','none');
	$('#opciones').css('display','none');
}

function calcularFechas(){
	var fecha = $('#fecha').val();
	var nombre = $('#name').val();
	var apellidos=$('#last').val();

	return $.ajax({
    	url:"persona.php",
    	method:'post',
    	data:{'funcion':"getFechas", 'fecha':fecha, 'nombre':nombre, 'apellidos':apellidos}
    }).done(function(respuesta){
    	h = respuesta;
    	 console.log(h);
    });  
}

function calcularSalario(){
	var fecha = $('#fecha').val();
	var nombre = $('#name').val();
	var apellidos=$('#last').val();

	var cargo = $('#cargo').val();
	var fechaIn=$('#fechaIn').val();
	var salario=$('#salario').val();

	return $.ajax({
		url:"persona.php",
		method:'post',
		data:{'funcion':'getSalario','fecha':fecha, 'nombre':nombre, 'apellidos':apellidos, 'cargo':cargo, 'fechaIn':fechaIn, 'salario':salario}
	}).done(function(respuesta){
		h = respuesta;
		console.log(h);
	});
}