<?php


	if(isset($_POST['funcion'])) {
			$funcion= $_POST['funcion'];
			$fecha=$_POST['fecha'];
			$nombre = $_POST['nombre'];
			$apellidos= $_POST['apellidos'];
			switch ($funcion) {
				case 'getFechas':
				$people = new Persona();
					$people->setDatos($nombre, $apellidos, $fecha);
					$edad =	$people->edad($fecha);
					$diasFecha = $people->diasproximocumpleanos($fecha);
					echo "{
						nombre:'".$nombre.",'
						apellidos:'".$apellidos.",'
						fecha:'".$fecha.",'
						edad:'".$edad.",'
						diasFaltantes:'".$diasFecha."'
					}";

					break;
				case 'getSalario':
					$cargo=$_POST['cargo'];
					$salario=$_POST['salario'];
					$fechaIn=$_POST['fechaIn'];

					$worker = new Empleado();
					$worker->setDatos($nombre, $apellidos, $fecha);
					$worker->setTrabajo($cargo, $salario, $fechaIn);
					$edad = $worker->edad($fecha);
					$bono = $worker->calculoSueldo();

					echo "{
						nombre:'".$nombre.",'
						apellidos:'".$apellidos.",'
						fecha Nacimiento:'".$fecha.",'
						edad:'".$edad.",'
						cargo:'".$cargo.",'
						fecha Ingreso:'".$fechaIn.",'
						salario:'".$salario.",'
						salario bono:'".$bono."'

					}";
					break;
				default:
					echo "default";
					break;
			}
		}
	
	class Persona{
		public $nombre ;
		public $apellidos;
		public $fecha_nacimiento;

		public function __construct(){
			$nombre = "";
			$apellidos="";
			$fecha_nacimiento="";
		}


		public function setDatos($name, $last, $birth){
			$this->nombre = $name;
			$this->apellidos = $last;
			$this->fecha_nacimiento = $birth;

		}

		public function edad($fecha){
			$cumple = new DateTime($fecha);
			$hoy = new DateTime();
			$ed = $hoy->diff($cumple);

			return $ed->y;

		}

		public function diasproximocumpleanos($fecha){
			//list($anio, $mes, $dia) = explode('-', $fecha);
			$arr =  explode('-', $fecha);

			$mes = $arr[1];
			$dia = $arr[2];
			$anio = $arr[0];

			$mesA= date("m");
			$diaA=date("d");
			$anioA=date("Y");

			$fechaActual=$anioA.'-'.$mesA.'-'.$diaA;
			$fechaCalcular='';
			 if($mesA > $mes){
			 	$anioA+=1;	
			 }else if($mesA==$mes){
			 	if($diaA > $dia){
			 		$anioA+=1;
			 	}
			 }
			$fechaCalcular=$anioA.'-'.$mes.'-'.$dia;

			$fechaActual=new DateTime($fechaActual);
			$fechaCalcular=new DateTime($fechaCalcular);

			$dias=$fechaCalcular->diff($fechaActual);

	        $dias_faltantes=($dias->days);

	        return $dias_faltantes;
		}

	}

	class Empleado extends Persona{
		public $cargo;
		public $salario;
		public $fechaIn;

		public function __construct(){
			$cargo="";
			$salario="";
			$fechaIn="";
		}

		public function setTrabajo($car, $sal, $fec){
			$this->cargo=$car;
			$this->salario = $sal;
			$this->fechaIn=$fec;


		}

		public function calculoSueldo(){
			$servicios=$this->edad($this->fechaIn);
			$edadWorker=$this->edad($this->fecha_nacimiento);
			$salariobono;
			if($servicios < 3){
				$salariobono = ($this->salario * 0.20);	
			}else{
				$salariobono=($this->salario * 0.40);

			}

			if($edadWorker>30){
					$salariobono+=100000;
				}

			return $salariobono;

		}
	}

?>